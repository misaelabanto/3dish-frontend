import './scss/main.scss'
import { db } from './constants/db';

console.log('Hello World from view main file! Khoo');


function main(): void {
	const businessName: string = location.hostname.split('.')[0];

	const business = db[businessName];

	const scene = document.querySelector('#scene');

	console.log({businessName});
	
	const marker = document.querySelector('#marker-camera');
	marker.setAttribute('url', business.marker);
	
	const entity = document.querySelector('#entity');
	entity.setAttribute('gltf-model', business.model);
}

window.onload = () => {
	main();
}
