export const db: any = {
	'rustica': {
		logo: 'https://3dish.s3.amazonaws.com/rustica.png',
		marker: 'https://3dish.s3.amazonaws.com/rustica.patt',
		model: 'https://3dish.s3.amazonaws.com/escabeche-de-pescado.glb'
	},
	'puntoazul': {
		logo: 'https://3dish.s3.amazonaws.com/punto_azul.png',
		marker: 'https://3dish.s3.amazonaws.com/punto-azul.patt',
		model: 'https://3dish.s3.amazonaws.com/escabeche-de-pescado.glb'
	},
	'localhost': {
		logo: 'https://3dish.s3.amazonaws.com/punto_azul.png',
		marker: 'https://3dish.s3.amazonaws.com/punto-azul.patt',
		model: 'https://3dish.s3.amazonaws.com/escabeche-de-pescado.glb'
	}
}